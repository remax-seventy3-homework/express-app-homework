# RemaxTech Interview Homework - Small Express App

This is a quick assignment to gauge your skills with Node.js, Express, and Elasticsearch. A person familiar with these tools should be able to complete this in about an hour, but take your time and feel free to look up anything you don't know.

**Please commit your code to a repository on [Gitlab](https://gitlab.com/)** and send the link when you are finished. We use this source control service for our production code and it will be helpful for you to be familiar with it.

**We (Remax) will not be sending real requests**, so it does not need to be deployed to an actual server, and there does not need to be an actual database in place. Rather, **we will be evaluating the code as written** for things like functionality, code cleanliness, and so on.

To reiterate, please do not spend your time setting up a live server or database.

## Instructions

Build an API that can get and save records in the `listings` elasticsearch index. Make sure credentials are not stored in the API code but instead can be accessed from the environment.

## Requirements

Use these node packages (required) and feel free to use any others you wish (optional) to complete the tasks below. Bear in mind that simplicity is good.

-   [express](https://expressjs.com/)
-   [dotenv](https://www.npmjs.com/package/dotenv)
-   [elasticsearch client](https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/index.html)

Write API endpoints to manage CRUD operations for the `listings` index

-   build a GET endpoint for all listings (limited to 20, excluding deleted listings)
-   build a POST endpoint to create a new listing from request body json (see sample data provided)

Make a `.env` file and bring ES connection credentials into the app from it

-   In a real app, we would not commit this file due to sensitive contents, but for this exercise, just commit it with the rest of your code

Use elasticsearch client to handle the CRUD operations of the endpoints

-   The code must be completed as if there were a live database connected

## Sample POST data

```json
{
    "address": "123 Fake Street, Anytown, NY 12345",
    "ageInDays": 25,
    "isDeleted": false,
    "propertyType": "House"
}
```

## Stretch Goals

If you're on a roll and just can't stop yourself, feel free to add any or all of these pieces of functionality as well (not required!)

-   build a GET endpoint to retrieve one listing (found by `_id`)
-   build a PATCH endpoint to modify an existing listing (found by `_id`) with arbitrary body json (`_id` cannot be modified)
-   build a DELETE endpoint to mark a listing (found by `_id`) as deleted (do not delete record from db)
